#!/usr/bin/env node
const prompts = require("prompts");

(async () => {
  const response = await prompts({
    type: "list",
    name: "number",
    message:
      "Enter digit of unorder numbers (separated by space, ex: 4 9 7 5 8 9 3)",
    initial: "4 9 7 5 8 9 3",
    separator: " ",
  });

  const arrayNumbers = response.number;
  console.log(`Skill Test - Sorting`);

  console.log(`Input number: ${arrayNumbers.join(" ")}`);

  //based on the first question, the sorting algorithm is bubble sort
  bubbleSort(arrayNumbers);

  function bubbleSort(arr) {
    //variable for marking swap is already done
    var swapped;
    //variable for counting swap
    var swapCount = 0;
    //do bubble sort (compares adjacent elements and swaps them if they are in the wrong order)
    //do until after check all array variable swapped keep false
    do {
      //mark swapped is not happen
      swapped = false;

      //check all array elements
      for (var i = 0; i < arr.length - 1; i++) {
        //compares adjacent elements
        if (arr[i] > arr[i + 1]) {
          //swap the wrong order
          var temp = arr[i];
          arr[i] = arr[i + 1];
          arr[i + 1] = temp;

          //swap is done between 2 number
          swapped = true;

          //count swap
          swapCount++;

          //print swap process
          console.log(
            `${swapCount}. [${arr[i]},${arr[i + 1]}] -> ${arr.join(" ")}`
          );
        }
      }
    } while (swapped);
    console.log("");
    //print swap amount
    console.log(`Jumlah swap: ${swapCount}`);
  }
})();
