# Skill Test - Sorting

In this part of the test, I made some program to do bubble sort and count how many swap that happened while the sort process. I made the program using javascript and node js. So this program will be running on CLI (terminal on command prompt).

### Installation

Install the program

```sh
$ npm install
OR
$ npm install -g
```

### Run Program

Run the program by execute command `sorttest`

```sh
$ sorttest
```

### Demo Program

```sh
$ sorttest
? Enter digit of unorder numbers (separated by space, ex: 4 9 7 5 8 9 3) › 4 9 7 5 8 9 3
Skill Test - Sorting
Input number: 4 9 7 5 8 9 3
1. [7,9] -> 4 7 9 5 8 9 3
2. [5,9] -> 4 7 5 9 8 9 3
3. [8,9] -> 4 7 5 8 9 9 3
4. [3,9] -> 4 7 5 8 9 3 9
5. [5,7] -> 4 5 7 8 9 3 9
6. [3,9] -> 4 5 7 8 3 9 9
7. [3,8] -> 4 5 7 3 8 9 9
8. [3,7] -> 4 5 3 7 8 9 9
9. [3,5] -> 4 3 5 7 8 9 9
10. [3,4] -> 3 4 5 7 8 9 9

Jumlah swap: 10
```
